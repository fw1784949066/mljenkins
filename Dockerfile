FROM python:3.8

RUN pip install joblib torch torchvision numpy -i http://pypi.douban.com/simple/ --trusted-host pypi.douban.com
RUN pip install minio -i http://pypi.douban.com/simple/ --trusted-host pypi.douban.com
USER root
RUN apt-get update && apt-get install -y jq

RUN mkdir data

COPY . .
